import { TarjetaDescuento } from './TarjetaDescuento';
import { TarjetaNormal } from './TarjetaNormal';
import { Descuento } from './Descuento';



const tarj1 = new TarjetaDescuento();
tarj1.setPrecio(1800);
const tarj2 = new TarjetaNormal();
tarj2.setPrecio(1800);

const descuento = new Descuento()
const result1 = tarj1.aceptar(descuento);
const result2 = tarj2.aceptar(descuento);

console.log(result1);
console.log(result2);