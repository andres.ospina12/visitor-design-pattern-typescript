import { Visitable } from './Visitable';
import { Visitor } from './Visitor';
export class TarjetaDescuento implements Visitable {
    precio: number;
    aceptar(visitor: Visitor) {
        return visitor.visitB(this)
    }
    getPrecios() {
        return this.precio;
    }

    setPrecio(precio: number) {
        this.precio = precio
    }
}