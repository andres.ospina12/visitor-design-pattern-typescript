import { TarjetaDescuento } from "./TarjetaDescuento";
import { TarjetaNormal } from "./TarjetaNormal";

export interface Visitor {
    visitA(normal: TarjetaNormal): void
    visitB(descuento: TarjetaDescuento): void
}