import { Visitor } from './Visitor'
export interface Visitable {
    aceptar(visitante: Visitor)
}