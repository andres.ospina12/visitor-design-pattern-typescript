import { TarjetaDescuento } from './TarjetaDescuento';
import { TarjetaNormal } from './TarjetaNormal';
import { Visitor } from './Visitor'
export class Descuento implements Visitor {
    sinDescuento: number = 0.8333333333;
    conDescuento: number = 0.3333333333;

    visitA(normal: TarjetaNormal) {
        return normal.getPrecios() * this.sinDescuento;
    }

    visitB(descuento: TarjetaDescuento) {
        return descuento.getPrecios() * this.conDescuento;
    }

}